#!/usr/bin/python3

import sys, os
import flask

app = flask.Flask('app')

@app.route('/', methods=['GET'])
def index():
    print(u'Базовая проверка АПИ.')
    return 'from root page\n'

@app.route('/tv_collector', methods=['POST'])
def tv_coll():
    info = flask.request.files.get('datafile', None)
    if info:
        fullname = os.getcwd() + '/storage/' + info.filename
        info.save(fullname)
        print(flask.request.cookies)
        print(flask.request.headers)
        if os.path.exists(fullname):
            res = os.stat(fullname).st_size
            return ('File %s saved. Size: %s' % (info.filename, res) , 200)
    return ('File no found', 400)

@app.route('/tv_list', methods=['GET'])
def tv_list():
    info = '\n'.join(os.listdir('data'))
    return info

if __name__ == '__main__':
    app.run(host='localhost', port=8500, threaded=True)

#, Debug=True)
#@app.register.route('get_tv', '/tv_collector')
#@app.register.route('tv_list', '/tv_list')k07ter@newbie1:~/task$ 
