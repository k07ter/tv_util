#!/usr/bin/python3

import sys, os, json, yaml
import mariadb, argparse, requests
import time
from datetime import datetime, timedelta
from crontab import CronTab

try:
    cnf = yaml.unsafe_load(open('settings.cnf'))
    db_info = cnf['auth']
    api = cnf['api_url']
    API_TOKEN = api['token']
except Exception as e:
    print('Check your settings file.')
    sys.exit(1)

DB_MASK = 'stat_y%sm%s'  # statistic database mask
STATISTIC_FOLDER = 'data/'


class TvTraffic(object):
    city_ids = [13, 21, 237]   # enabled city bases
    datafile_mask = STATISTIC_FOLDER + '%s_%s.json'

    def __init__(self, db_name):
        self.db_info = db_info
        self.conn = self.c2d(db_name)
        self.city_traf = {}

    def c2d(self, db_name='mysql'):
        ''' Function create connection to database and return it for using
        '''
        self.db_info['database'] = db_name
        try:
            db_conn = mariadb.connect(**self.db_info)
        except mariadb.Error as e:
            print('Connection error ->', e)
            return
        return db_conn

    def set_dbName(db_name):
        self.conn.database = db_name

    def get_tvFullInfo(self):
        ''' Collection information about viewing channels for each town and return dictionary.
            mn and yr - for getting database name from mask
        '''
        db_cur = self.conn.cursor()
        QUERY = "select geoID, tvID, mh_city_id_timeshift from otr.otr_map order by geoID"
        db_cur.execute(QUERY)

        records = [r for r in db_cur.fetchall() if r[0] in self.city_ids]
        return records

    def get_tvInfoByCity(self, mn=None, yr=None):
        ''' Collection information about viewing channels for each town and return dictionary.
            mn and yr - for getting database name from mask
        '''
        #ENABLE_TOWNS = self.city_ids  # exists town databases
        self.conn.database = DB_MASK % (yr, mn)
        QUERY = "select geoID, tvID, mh_city_id_timeshift from otr.otr_map order by geoID"
        curDB = self.conn.cursor()
        curDB.execute(QUERY)
        town_map = {} 
        _cursor = curDB.fetchall()

        for r in _cursor:
            if r[0] in self.city_ids:
                geo_shft = (r[0], r[2])
                _t_vlu = town_map.get(geo_shft)
                if _t_vlu is None:
                    town_map[geo_shft] = [r[1]]
                else:
                    town_map[geo_shft].append(r[1])
        return town_map

    def collect_tvInfo(self, town_map, target_day):
        '''  Getting information about viewing channels for a target date by minutes
            - target_day: date for request
            - town_map: dictionary with a set of channels and shift time for each town
        '''
        result = []
        DBNAME = self.conn.database
        curDB = self.conn.cursor()

        TABLE_NAME_MASK = '{0}.{0}_geo{1}'
        DAY_RANGE_MASK = "'{0} 00:00:00' and '{0} 23:59:59'"
        QUERY_LINE = "select b.num_key, a.sTimeMsk, a.dur from %s a " +\
                     "join rt.num_keys b on b.num=a.num " + \
                     "where 1 and sTimeMsk between %s and tvID in (%s)"

        for (geoID_shftTime, tvIDs) in town_map.items():
            gID, shTm = geoID_shftTime
            TABLE_NAME = TABLE_NAME_MASK.format(DBNAME, gID)
            DAY_RANGE = DAY_RANGE_MASK.format(target_day)
            QUERY = QUERY_LINE % (TABLE_NAME, DAY_RANGE, ('%s' % tvIDs)[1:-1])
            curDB.execute(QUERY)
            records = curDB.fetchall()

            for rec in records:
                num_key, sTimeMsk, dur = rec
                _shftTm = sTimeMsk + timedelta(seconds=dur)

                # iterating through all minutes in range
                for m in range(sTimeMsk.minute, _shftTm.minute + 1):
                    local_time = self.date_to_line(sTimeMsk, m, h=shTm)
                    moscow_time = self.date_to_line(sTimeMsk, m)

                    for ech in result:
                        if ech.get('moscow_datetime', '') == moscow_time:
                            ech['num_keys'].append(num_key)

                    details = {
                        'geoID': gID,
                        'local_datetime': local_time,
                        'moscow_datetime': moscow_time,
                        'num_keys': [num_key]
                    }
                    result.append(details)
        return result

    @staticmethod
    def date_to_line(data, m, h=None):
        ''' Changing the value of time and converting a date to a string
        '''
        if h:
            data = data + timedelta(hours=h)
        data = data.replace(minute=m)
        data_line = data.replace(second=0).strftime('%Y-%m-%d %H:%M:%S')
        return data_line

    def split_by_city(self, town_info):
        '''
            Spliting information by cities and saving
        '''
        [os.remove(os.getcwd() + '/' + STATISTIC_FOLDER + r) for r in os.listdir('./' + STATISTIC_FOLDER)]
        for elm in town_info:
            fname_geo = self.datafile_mask % (elm['moscow_datetime'][:10], elm['geoID'])
            with open(fname_geo, 'a+') as fp:
                json.dump(elm, fp, sort_keys=True)
        print('done')

    def upload_2url(self, url_addr, datafile):
            sess = requests.Session()
            headers={'Authorization': API_TOKEN}

            #for f in files_to_upload:
            try:
                #fls_2_upl = [open(f, 'rb') for f in files_to_upload]
                #ind = files_to_upload.index(f) + 1
                #datafile = self.datafile_mask % f
                files = {'datafile': open(datafile, 'rb')}
                resp = sess.post(url_addr, headers=headers, files=files)
                print(resp.content, resp.status_code)
                return resp.content.decode(), resp.status_code
            except Exception as e:
                print(e)
                return

def plain_to_sched():
    ''' Adding the script start time to scheduler
    '''
    cron = CronTab(user=os.getlogin())
    print(sys.argv)
    if len(sys.argv[1:]) == 2:
        hr, mn = sys.argv[-1].split('.')
        command='nohup python3 %s/%s' % (os.getcwd(), __file__)
        print(command)
        #cron.new(command=command)
    print('Executed by scheduler')

def restart_proc():
    print('To restart')

def main():
    print('app launched ...')

    YEAR = '2021'
    MONTH = '02'
    TARGET_DAY = '2021-02-01'

    tvt = TvTraffic('otr')
    # getting information about cities
    channels_info = tvt.get_tvFullInfo()

    # getting info about a viewed channels for ech city 
    viewed_tvIDs = tvt.get_tvInfoByCity(mn=MONTH, yr=YEAR)

    # about all channels for ech minute
    tv_inf = tvt.collect_tvInfo(viewed_tvIDs, TARGET_DAY)

    # saving info to file
    tvt.split_by_city(tv_inf)

    # build address of api url
    API_URL = api['base_addr'] + api['pref']
    for ech in os.listdir('./' + STATISTIC_FOLDER):
        fname = os.getcwd() + '/' + STATISTIC_FOLDER + ech
        #[ech for ech in os.listdir('./' + STATISTIC_FOLDER)])
        s2u = tvt.upload_2url(API_URL, fname)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script for collecting TV-statistic')
    parser.add_argument('--sched', '-s', help='Add start time to scheduler: -s hr.mn ')
    parser.add_argument('--restart', '-r', action='store_true', help='To restart script')
    args = parser.parse_args()

    if len(sys.argv) == 1:
        main()

    if '-r' in sys.argv:
        restart_proc()
    if '-s' in sys.argv:
        plain_to_sched()
